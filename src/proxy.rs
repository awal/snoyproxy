use crate::util::{remote_addr_from_host, split_url};
use crate::{config::Config, request::Request, tcp_stream_reader::TcpStreamReader};
use async_std::sync::RwLock;
use async_std::{
    io::{ReadExt, WriteExt},
    net::TcpStream,
};
use futures::{select, FutureExt};
use std::io::Error;
use std::sync::Arc;
use std::time::Duration;

type RemoteAddr<'a> = (&'a str, u16);

#[derive(PartialEq, Debug)]
enum ProxyMethod<'a> {
    Tunnel(RemoteAddr<'a>),
    Http(RemoteAddr<'a>, &'a [u8]),
}

impl ProxyMethod<'_> {
    fn remote_port(&self) -> u16 {
        match self {
            ProxyMethod::Tunnel(addr) => addr.1,
            ProxyMethod::Http(addr, _) => addr.1,
        }
    }
}

const RESPONSE_AUTH_REQUIRED: &[u8] = b"HTTP/1.0 407 Proxy Authentication Required\r
Server: Snoyproxy/0.0.1\r
Proxy-Authenticate: Basic realm=\"Snoyproxy\"\r
\r
";

const RESPONSE_BAD_REQUEST: &[u8] = b"HTTP/1.0 400 Bad Request\r
Server: Snoyproxy/0.0.1\r
\r
";

const RESPONSE_CONNECT_SUCCESS: &[u8] = b"HTTP/1.0 200 Connection Established\r
Server: Snoyproxy/0.0.1\r
\r
";

const RESPONSE_CONNECT_FAILED: &[u8] = b"HTTP/1.0 500 Connect Failed\r
Server: Snoyproxy/0.0.1\r
\r
";

pub async fn proxy<'a>(
    request: &'a Request,
    client_stream: &'a mut TcpStreamReader<'a>,
    config: Arc<RwLock<Config>>,
) -> Result<(), Error> {
    if config.read().await.basic_auth.is_some()
        && !config
            .read()
            .await
            .should_allow_credentials(request.get_basic_auth_credentials())
    {
        client_stream.src.write_all(RESPONSE_AUTH_REQUIRED).await?;
        client_stream.src.shutdown(std::net::Shutdown::Both)?;
        return Ok(());
    }

    let proxy_method = match get_proxy_method(request) {
        Some(proxy_method) => proxy_method,
        None => {
            client_stream.src.write_all(RESPONSE_BAD_REQUEST).await?;
            client_stream.src.shutdown(std::net::Shutdown::Both)?;
            return Ok(());
        }
    };

    if !config
        .read()
        .await
        .should_allow_port(proxy_method.remote_port())
    {
        client_stream.src.shutdown(std::net::Shutdown::Both)?;
        return Ok(());
    }

    let timeout = config.read().await.timeout;
    proxy_inner(request, proxy_method, client_stream, timeout).await
}

async fn proxy_inner<'a>(
    request: &'a Request,
    proxy_method: ProxyMethod<'a>,
    client_stream: &'a mut TcpStreamReader<'a>,
    timeout: Duration,
) -> Result<(), Error> {
    match proxy_method {
        ProxyMethod::Tunnel(remote_addr) => proxy_tunnel(client_stream, remote_addr, timeout).await,
        ProxyMethod::Http(remote_addr, http_path) => {
            proxy_http(request, client_stream, remote_addr, http_path, timeout).await
        }
    }
}

async fn proxy_tunnel<'a>(
    client_stream: &'a mut TcpStreamReader<'a>,
    remote_addr: RemoteAddr<'a>,
    timeout: Duration,
) -> Result<(), Error> {
    let remote_stream = TcpStream::connect(remote_addr).await;

    match remote_stream {
        Ok(remote_stream) => {
            client_stream
                .src
                .write_all(RESPONSE_CONNECT_SUCCESS)
                .await?;
            relay(client_stream, remote_stream, timeout).await
        }
        Err(err) => {
            client_stream.src.write_all(RESPONSE_CONNECT_FAILED).await?;
            Err(err)
        }
    }
}

async fn proxy_http<'a>(
    request: &'a Request,
    client_stream: &'a mut TcpStreamReader<'a>,
    remote_addr: RemoteAddr<'a>,
    http_path: &'a [u8],
    timeout: Duration,
) -> Result<(), Error> {
    let remote_stream = TcpStream::connect(remote_addr).await;

    match remote_stream {
        Ok(mut remote_stream) => {
            remote_stream.write_all(&request.method).await?;
            remote_stream.write_all(b" ").await?;
            remote_stream.write_all(http_path).await?;
            remote_stream.write_all(b" ").await?;
            remote_stream.write_all(&request.protocol).await?;
            remote_stream.write_all(b"\r\n").await?;

            remote_stream.write_all(b"Host: ").await?;
            remote_stream.write_all(remote_addr.0.as_bytes()).await?;
            remote_stream.write_all(b"\r\n").await?;

            for (header_name, header_value) in &request.headers {
                if header_name.eq_ignore_ascii_case(b"host")
                    || header_name
                        .get(..8)
                        .map_or(false, |prefix| prefix.eq_ignore_ascii_case(b"proxy-"))
                {
                    continue;
                }
                remote_stream.write_all(header_name).await?;
                remote_stream.write_all(b":").await?;
                remote_stream.write_all(header_value).await?;
                remote_stream.write_all(b"\r\n").await?;
            }

            remote_stream.write_all(b"\r\n").await?;

            relay(client_stream, remote_stream, timeout).await
        }
        Err(err) => {
            client_stream.src.write_all(RESPONSE_CONNECT_FAILED).await?;
            Err(err)
        }
    }
}

fn get_proxy_method(request: &Request) -> Option<ProxyMethod<'_>> {
    if request.method == b"CONNECT" || request.method == b"connect" {
        if let Some(remote_addr) = remote_addr_from_host(&request.path) {
            return Some(ProxyMethod::Tunnel(remote_addr));
        }
    }

    let (remote_addr, path) = if request.path.starts_with(b"http://") {
        split_url(&request.path)?
    } else {
        (
            remote_addr_from_host(request.get_header(b"host")?)?,
            &request.path[..],
        )
    };

    Some(ProxyMethod::Http(remote_addr, path))
}

async fn relay<'a>(
    client_stream: &'a mut TcpStreamReader<'a>,
    mut remote_stream: TcpStream,
    timeout: Duration,
) -> Result<(), Error> {
    // write any remaining data in the client stream that we read but wasn't part of the headers
    remote_stream.write_all(&client_stream.buf).await?;

    let client_stream = &mut client_stream.src;

    let (mut client_reader, mut client_writer) = (client_stream.clone(), client_stream.clone());
    let (mut remote_reader, mut remote_writer) = (remote_stream.clone(), remote_stream.clone());

    let mut client_buf = vec![0; 2048];
    let mut remote_buf = vec![0; 2048];

    loop {
        select! {
            client_bytes_read = async_std::io::timeout(timeout, client_reader.read(&mut client_buf)).fuse() => match client_bytes_read {
                Ok(bytes_read) => {
                    if bytes_read == 0 {
                        return Ok(());
                    }
                    remote_writer.write_all(&client_buf[..bytes_read]).await?;
                },
                err @ Err(_) => { err?; } // XXX: send log somewhere?
            },
            remote_bytes_read = async_std::io::timeout(timeout, remote_reader.read(&mut remote_buf)).fuse() => match remote_bytes_read {
                Ok(bytes_read) => {
                    if bytes_read == 0 {
                        return Ok(());
                    }
                    client_writer.write_all(&remote_buf[..bytes_read]).await?;
                },
                err @ Err(_) => { err?; } // XXX: send log somewhere?
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_proxy_method() {
        let request = Request {
            method: "GET".into(),
            path: "/foo/bar".into(),
            protocol: "HTTP/1.1".into(),
            headers: vec![("host".into(), "example.com:8080".into())],
        };
        assert_eq!(
            get_proxy_method(&request),
            Some(ProxyMethod::Http(("example.com", 8080), &b"/foo/bar"[..]))
        );

        let request = Request {
            method: "CONNECT".into(),
            path: "example.com:443".into(),
            protocol: "HTTP/1.1".into(),
            headers: vec![("host".into(), "example.com:8080".into())],
        };
        assert_eq!(
            get_proxy_method(&request),
            Some(ProxyMethod::Tunnel(("example.com", 443)))
        );

        let request = Request {
            method: "GET".into(),
            path: "http://badpath:invalid".into(),
            protocol: "HTTP/1.1".into(),
            headers: vec![("host".into(), "example.com:8080".into())],
        };
        assert_eq!(get_proxy_method(&request), None);

        let request = Request {
            method: "GET".into(),
            path: "http://example.com/foo/bar".into(),
            protocol: "HTTP/1.1".into(),
            headers: vec![("host".into(), "fake.com".into())],
        };
        assert_eq!(
            get_proxy_method(&request),
            Some(ProxyMethod::Http(("example.com", 80), &b"/foo/bar"[..]))
        );

        let request = Request {
            method: "GET".into(),
            path: "/".into(),
            protocol: "HTTP/1.1".into(),
            headers: vec![],
        };
        assert_eq!(get_proxy_method(&request), None);

        let request = Request {
            method: "GET".into(),
            path: "/".into(),
            protocol: "HTTP/1.1".into(),
            headers: vec![("host".into(), "example.com:invalid".into())],
        };
        assert_eq!(get_proxy_method(&request), None);
    }
}

mod config;
mod proxy;
mod request;
mod tcp_stream_reader;
mod util;

use async_std::net::{TcpListener, TcpStream};
use async_std::stream::StreamExt;
use async_std::sync::RwLock;
use config::Config;
use futures::{select, FutureExt};
use getopts::Options;
use std::path::Path;
use std::{net::SocketAddr, sync::Arc};

use crate::proxy::proxy;
use crate::{request::Request, tcp_stream_reader::TcpStreamReader};

#[async_std::main]
async fn main() {
    let argv_config_path = process_args();
    run(argv_config_path).await;
}

async fn run(argv_config_path: Option<String>) {
    let config = match load_config(&argv_config_path) {
        Ok(config) => config,
        Err(err) => {
            eprintln!("Failed to load config: '{}'", err);
            std::process::exit(2);
        }
    };

    println!("Loaded config: {:#?}", config);

    let mut listener = match TcpListener::bind((config.listen, config.port)).await {
        Ok(listener) => listener,
        Err(err) => {
            eprintln!(
                "Failed to bind on '{}:{}': '{}'",
                config.listen, config.port, err
            );
            std::process::exit(2);
        }
    };

    let mut signals = match async_signals::Signals::new(vec![libc::SIGUSR1]) {
        Ok(signals) => signals,
        Err(err) => {
            eprintln!("Failed to listen on signals: '{}'", err);
            std::process::exit(2);
        }
    };

    let config = Arc::new(RwLock::new(config));

    loop {
        select! {
            incoming = listener.accept().fuse() => match incoming {
                Ok((socket, peer)) => {
                    let config = config.clone();
                    async_std::task::spawn(async move {
                        serve_peer(socket, peer, config).await;
                    });
                }
                Err(err) => {
                    eprintln!("Failed to accept connection from listener: '{}'", err);
                    std::process::exit(3);
                }
            },
            signal = signals.next().fuse() => if let Some(libc::SIGUSR1) = signal {
                println!("Reloading config...");
                let config = config.clone();
                let mut config = config.write().await;
                let old_addr = (config.listen, config.port);
                let new_config = match load_config(&argv_config_path) {
                    Ok(new_config) => new_config,
                    Err(err) => {
                        eprintln!("Failed to reload config: '{}'", err);
                        continue;
                    },
                };
                *config = new_config;
                println!("Loaded new config: {:#?}", &config);
                let new_addr = (config.listen, config.port);
                if old_addr != new_addr {
                    listener = TcpListener::bind(new_addr).await.expect("rebind");
                }
            }
        }
    }
}

async fn serve_peer(mut socket: TcpStream, peer: SocketAddr, config: Arc<RwLock<Config>>) {
    if !config.read().await.should_allow_peer(&peer.ip()) {
        return match socket.shutdown(std::net::Shutdown::Both) {
            Ok(_) => {}
            Err(err) => {
                eprintln!("Failed to shutdown peer: '{}'", err);
            }
        };
    }

    let mut reader = TcpStreamReader::new(&mut socket);
    let request = match async_std::io::timeout(
        config.read().await.timeout,
        Request::from_stream_reader(&mut reader),
    )
    .await
    {
        Ok(request) => request,
        Err(err) => {
            eprintln!("Error parsing request header: '{}'", err);
            return;
        }
    };

    match proxy(&request, &mut reader, config).await {
        Ok(_) => {}
        Err(err) => {
            eprintln!("Error occurred while proxying: '{}'", err);
        }
    };
}

fn process_args() -> Option<String> {
    let args: Vec<String> = std::env::args().collect();
    let program = Path::new(&args[0])
        .file_name()
        .expect("program name")
        .to_str()
        .expect("program name");

    let mut opts = getopts::Options::new();
    opts.optflag("v", "version", "Display version and exit");
    opts.optopt("c", "config", "Set path to config file", "CONFIG");
    opts.optflag("h", "help", "Display help and exit");
    opts.optflag("d", "", "Ignored (for compat with tinyproxy)");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(_) => {
            print_usage(program, opts);
            std::process::exit(1);
        }
    };

    if matches.opt_present("h") {
        print_usage(program, opts);
        std::process::exit(0);
    }

    if matches.opt_present("v") {
        println!("{}", env!("CARGO_PKG_VERSION"));
        std::process::exit(0);
    }

    matches.opt_str("c")
}

fn load_config(argv_config_path: &Option<String>) -> Result<Config, String> {
    if let Some(argv_config_path) = argv_config_path {
        Config::from_path(argv_config_path)
    } else if let Some(path) = ["/etc/snoyproxy.conf", "/etc/tinyproxy/tinyproxy.conf"]
        .into_iter()
        .find(|path| std::path::Path::new(path).exists())
    {
        Config::from_path(path)
    } else {
        Err("No config file found".to_string())
    }
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}

use base64::Engine;
use bstr::ByteSlice;

use crate::tcp_stream_reader::TcpStreamReader;
use std::io::Error;

pub struct Request {
    pub method: Vec<u8>,
    pub path: Vec<u8>,
    pub protocol: Vec<u8>,
    pub headers: Vec<(Vec<u8>, Vec<u8>)>,
}

impl Request {
    pub async fn from_stream_reader<'a>(reader: &mut TcpStreamReader<'a>) -> Result<Self, Error> {
        let method = reader.read_until(b' ', Some(32)).await?;
        let path = reader.read_until(b' ', Some(4096)).await?;
        let mut protocol = reader.read_until(b'\n', Some(10)).await?;

        if protocol[protocol.len() - 1] == b'\r' {
            protocol.pop();
        }

        let mut headers = vec![];

        loop {
            if reader.peek(1).await? == b"\n" {
                reader.skip(1).await?;
                break;
            }

            if reader.peek(2).await? == b"\r\n" {
                reader.skip(2).await?;
                break;
            }

            let header_name = reader.read_until(b':', Some(128)).await?;
            let mut header_value = reader.read_until(b'\n', Some(4096)).await?;

            if header_value[header_value.len() - 1] == b'\r' {
                header_value.pop();
            }

            headers.push((header_name, header_value));
        }

        Ok(Self {
            method,
            path,
            protocol,
            headers,
        })
    }

    pub fn get_header(&self, header_name: &[u8]) -> Option<&[u8]> {
        for (name, value) in &self.headers {
            if name.eq_ignore_ascii_case(header_name) {
                return Some(value);
            }
        }
        None
    }

    pub fn get_basic_auth_credentials(&self) -> Option<String> {
        let auth = self.get_header(b"Proxy-Authorization")?.to_str().ok()?;
        match auth
            .trim()
            .splitn(2, char::is_whitespace)
            .collect::<Vec<_>>()[..]
        {
            [mode, credentials] if mode.trim().eq_ignore_ascii_case("basic") => {
                base64::engine::general_purpose::STANDARD
                    .decode(credentials)
                    .map(String::from_utf8)
                    .map(Result::ok)
                    .ok()
                    .flatten()
            }
            _ => None,
        }
    }
}

impl std::fmt::Debug for Request {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use bstr::BStr;

        f.debug_struct("Request")
            .field("method", &BStr::new(&self.method))
            .field("path", &BStr::new(&self.path))
            .field("protocol", &BStr::new(&self.protocol))
            .field(
                "headers",
                &self
                    .headers
                    .iter()
                    .map(|(name, value)| (BStr::new(name), BStr::new(value)))
                    .collect::<Vec<_>>(),
            )
            .finish()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_header() {
        let request = Request {
            method: vec![],
            path: vec![],
            protocol: vec![],
            headers: vec![
                ("Host".into(), "example.com:443".into()),
                ("Accept".into(), "text/html".into()),
            ],
        };

        assert_eq!(request.get_header(b"host"), Some(&b"example.com:443"[..]));
        assert_eq!(request.get_header(b"HoSt"), Some(&b"example.com:443"[..]));
        assert_eq!(request.get_header(b"location"), None);
    }

    #[test]
    fn test_get_basic_auth_credentials() {
        let mut request = Request {
            method: vec![],
            path: vec![],
            protocol: vec![],
            headers: vec![(
                "Proxy-Authorization".into(),
                "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==".into(),
            )],
        };

        assert_eq!(
            request.get_basic_auth_credentials(),
            Some("Aladdin:open sesame".into())
        );

        request.headers[0].1 = "Basic asfuisdgd".into();
        assert_eq!(request.get_basic_auth_credentials(), None);

        request.headers[0].1 = "asuifyrueg".into();
        assert_eq!(request.get_basic_auth_credentials(), None);

        request.headers.clear();
        assert_eq!(request.get_basic_auth_credentials(), None);
    }
}

pub fn split_quotable_token(mut s: &str) -> Option<(&str, &str)> {
    s = s.trim();
    if s.is_empty() {
        return None;
    }
    let token_end = {
        if s.starts_with('"') {
            s = &s[1..];
            s.find('"')?
        } else {
            s.find(char::is_whitespace).unwrap_or(s.len())
        }
    };
    let token = &s[..token_end];
    s = s.get(token_end + 1..).unwrap_or("");
    Some((token, s))
}

pub fn split_url(url: &[u8]) -> Option<((&str, u16), &[u8])> {
    split_url_inner(&url[7..]) // strip http://
}

// assumes that url is stripped of http://
fn split_url_inner(mut url: &[u8]) -> Option<((&str, u16), &[u8])> {
    if url.is_empty() {
        return None;
    }

    let mut host = url;
    let mut port_str = None;
    let mut path = None;
    let mut pass = 0;

    loop {
        if pass == 1 {
            pass = 2;
        } else if pass == 2 {
            break;
        }

        for (i, char) in url.iter().enumerate() {
            if char == &b'@' {
                url = &url[i + 1..];
                host = url;
                port_str = None;
                path = None;
                pass = 1;
                break;
            }
            if char == &b':' {
                host = &url[..i];
                port_str = Some(&url[i + 1..]);
            }
            if char == &b'/' {
                if port_str.is_some() {
                    port_str = Some(&url[host.len() + 1..i]);
                } else {
                    host = &url[..i];
                }
                path = Some(&url[i..]);
                break;
            }
        }

        if pass == 0 {
            break;
        }
    }

    let port = match port_str {
        Some(port_str) => bytes_to_port(port_str)?,
        None => 80,
    };

    let remote_addr = (std::str::from_utf8(host).ok()?, port);
    let path = path.unwrap_or(b"/");

    Some((remote_addr, path))
}

pub fn remote_addr_from_host(host: &[u8]) -> Option<(&str, u16)> {
    if host.is_empty()
        || host.len() > (/* max dns name length */253 + /* : */ 1 + /* max port length */ 5)
    {
        return None;
    }

    match host.splitn(2, |x| x == &b':').collect::<Vec<_>>()[..] {
        [host, port_str] => Some((std::str::from_utf8(host).ok()?, bytes_to_port(port_str)?)),
        [host] => Some((std::str::from_utf8(host).ok()?, 80u16)),
        _ => unreachable!(),
    }
}

fn bytes_to_port(sl: &[u8]) -> Option<u16> {
    bstr::ByteSlice::to_str_lossy(sl).parse().ok()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_split_quotable_token() {
        assert_eq!(split_quotable_token(""), None);
        assert_eq!(split_quotable_token("   "), None);
        assert_eq!(split_quotable_token("token"), Some(("token", "")));
        assert_eq!(split_quotable_token("\"token\""), Some(("token", "")));
        assert_eq!(
            split_quotable_token("\"token\" other_token"),
            Some(("token", " other_token"))
        );
        assert_eq!(
            split_quotable_token("\" token1 foo bar \"  \"token2\""),
            Some((" token1 foo bar ", "  \"token2\""))
        );
        assert_eq!(split_quotable_token("\"token foo"), None);
    }

    #[test]
    fn test_split_url() {
        assert_eq!(
            split_url(b"http://user:password@example.com:443/foo/bar"),
            Some((("example.com", 443), &b"/foo/bar"[..]))
        );
        assert_eq!(
            split_url(b"http://user:password@example.com:443"),
            Some((("example.com", 443), &b"/"[..]))
        );
        assert_eq!(
            split_url(b"http://user:password@example.com/foo/bar"),
            Some((("example.com", 80), &b"/foo/bar"[..]))
        );
        assert_eq!(
            split_url(b"http://user:password@example.com"),
            Some((("example.com", 80), &b"/"[..]))
        );
        assert_eq!(
            split_url(b"http://example.com:443/foo/bar"),
            Some((("example.com", 443), &b"/foo/bar"[..]))
        );
        assert_eq!(
            split_url(b"http://user@example.com:443/foo/bar"),
            Some((("example.com", 443), &b"/foo/bar"[..]))
        );
        assert_eq!(
            split_url(b"http://example.com/foo/bar"),
            Some((("example.com", 80), &b"/foo/bar"[..]))
        );
        assert_eq!(split_url(b"http://"), None);
    }

    #[test]
    fn test_remote_addr_from_host() {
        assert_eq!(remote_addr_from_host(b"http://example.com"), None);
        assert_eq!(remote_addr_from_host(b"https://example.com"), None);
        assert_eq!(
            remote_addr_from_host(b"example.com"),
            Some(("example.com", 80u16))
        );
        assert_eq!(
            remote_addr_from_host(b"example.com:443"),
            Some(("example.com", 443u16))
        );
        assert_eq!(remote_addr_from_host(b"example.com:invalid"), None);
    }
}

use std::{
    fs::File,
    io::{BufRead, BufReader},
    net::{IpAddr, Ipv4Addr},
    time::Duration,
};

use ipnet::IpNet;

use crate::util::split_quotable_token;

#[derive(Debug)]
pub struct Config {
    pub port: u16,
    pub listen: IpAddr,
    pub timeout: Duration,
    pub log_level: LogLevel,
    pub allow: Vec<IpOrSubnet>,
    pub deny: Vec<IpOrSubnet>,
    pub basic_auth: Option<String>,
    pub connect_ports: Vec<u16>,
}

#[derive(Debug)]
pub enum LogLevel {
    Critical,
    Error,
    Warning,
    Notice,
    Connect,
    Info,
}

impl std::str::FromStr for LogLevel {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_ascii_lowercase().as_str() {
            "critical" => Ok(LogLevel::Critical),
            "error" => Ok(LogLevel::Error),
            "warning" => Ok(LogLevel::Warning),
            "notice" => Ok(LogLevel::Notice),
            "connect" => Ok(LogLevel::Connect),
            "info" => Ok(LogLevel::Info),
            _ => Err(format!("Unknown log level {}", s)),
        }
    }
}

#[derive(Debug)]
pub enum IpOrSubnet {
    Ip(IpAddr),
    Subnet(IpNet),
}

impl std::str::FromStr for IpOrSubnet {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.parse::<IpNet>() {
            Ok(subnet) => Ok(Self::Subnet(subnet)),
            Err(ipnet_err) => match s.parse::<IpAddr>() {
                Ok(addr) => Ok(Self::Ip(addr)),
                Err(addr_err) => Err(format!(
                    "Failed to parse '{}' as subnet ({}) or ip address ({})",
                    s, ipnet_err, addr_err
                )),
            },
        }
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            port: 8080,
            listen: IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)),
            timeout: Duration::from_secs(10 * 60),
            log_level: LogLevel::Connect,
            allow: vec![],
            deny: vec![],
            basic_auth: None,
            connect_ports: vec![],
        }
    }
}

impl Config {
    pub fn from_path(path: &str) -> Result<Self, String> {
        let file = BufReader::new(
            File::open(path).map_err(|e| format!("Unable to open file {}, {}", path, e))?,
        );

        let mut config = Config::default();

        for (i, line) in file.lines().enumerate() {
            let line = line.map_err(|err| format!("invalid data on line {}, {}", i + 1, err))?;
            config
                .apply_config_line(&line)
                .map_err(|err| format!("error on line {}: {}", i + 1, err))?;
        }

        Ok(config)
    }

    fn apply_config_line(&mut self, line: &str) -> Result<(), String> {
        let line = line.trim();
        if line.is_empty() || line.starts_with('#') {
            return Ok(());
        }

        let args = line.splitn(2, char::is_whitespace).collect::<Vec<_>>();

        let [key, value] = args[..] else {
            return Err("missing argument".to_string());
        };

        let key = &key.to_ascii_lowercase()[..];
        let value = value.trim();

        match (key, value) {
            ("port", port) => {
                self.port = port
                    .parse()
                    .map_err(|_| format!("invalid port '{}'", port))?;
            }
            ("listen", ipaddr) => {
                self.listen = ipaddr
                    .parse()
                    .map_err(|_| format!("invalid listen directive '{}'", ipaddr))?;
            }
            ("timeout", timeout) => {
                self.timeout = Duration::from_secs(
                    timeout
                        .parse::<u64>()
                        .map_err(|_| format!("invalid timeout value '{}'", timeout))?,
                );
            }
            ("log_level", log_level) => {
                self.log_level = log_level
                    .parse()
                    .map_err(|_| format!("invalid log level '{}'", log_level))?;
            }
            ("allow", allow) => {
                self.allow.push(
                    allow
                        .parse()
                        .map_err(|e| format!("invalid allow rule '{}', '{}'", allow, e))?,
                );
            }
            ("deny", allow) => {
                self.deny.push(
                    allow
                        .parse()
                        .map_err(|e| format!("invalid deny rule '{}', '{}'", allow, e))?,
                );
            }
            ("basicauth", basic_auth) => {
                let (username, password) = parse_basic_auth(basic_auth)?;
                self.basic_auth = Some(format!("{}:{}", username, password));
                // we can't store base64 encoded credentials because base64 encoded strings are not comparable
            }
            ("connectport", port) => {
                self.connect_ports.push(
                    port.parse()
                        .map_err(|_| format!("invalid connect port '{}'", port))?,
                );
            }
            _ => {}
        };

        Ok(())
    }

    pub fn should_allow_peer(&self, peer: &IpAddr) -> bool {
        if self.allow.is_empty() && self.deny.is_empty() {
            return true;
        }

        let deny = does_peer_match_ruleset(peer, &self.deny);

        if deny {
            false
        } else if self.allow.is_empty() {
            !self.deny.is_empty()
        } else {
            does_peer_match_ruleset(peer, &self.allow)
        }
    }

    pub fn should_allow_port(&self, port: u16) -> bool {
        self.connect_ports.is_empty() || self.connect_ports.contains(&port)
    }

    pub fn should_allow_credentials(&self, given_credentials: Option<String>) -> bool {
        self.basic_auth.is_none() || self.basic_auth == given_credentials
    }
}

fn does_peer_match_ruleset(peer: &IpAddr, ruleset: &[IpOrSubnet]) -> bool {
    ruleset
        .iter()
        .any(|spec| does_peer_match_ip_or_subnet(peer, spec))
}

fn does_peer_match_ip_or_subnet(peer: &IpAddr, spec: &IpOrSubnet) -> bool {
    match spec {
        IpOrSubnet::Ip(addr) => addr == peer,
        IpOrSubnet::Subnet(subnet) => subnet.contains(peer),
    }
}

fn parse_basic_auth(input: &str) -> Result<(&str, &str), String> {
    let (user, input) =
        split_quotable_token(input).ok_or::<String>("username and password missing".into())?;
    let (password, trailing) =
        split_quotable_token(input).ok_or::<String>("password missing".into())?;
    if !trailing.trim().is_empty() {
        return Err("trailing data after username and password".into());
    }
    Ok((user, password))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_should_allow_peer() {
        let allow_rules = vec![
            "127.0.0.1".parse().unwrap(),
            "10.200.0.0/24".parse().unwrap(),
        ];

        let deny_rules = vec![
            "127.0.0.0/24".parse().unwrap(),
            "10.200.0.1".parse().unwrap(),
        ];

        let mut config = Config::default();
        assert!(config.should_allow_peer(&"127.0.0.1".parse().unwrap())); // with no rules, allow by default

        config.allow = allow_rules;
        config.deny = deny_rules;

        assert!(!config.should_allow_peer(&"127.0.0.1".parse().unwrap())); // deny rule takes precedence
        assert!(!config.should_allow_peer(&"10.200.0.1".parse().unwrap()));
        assert!(!config.should_allow_peer(&"127.0.0.5".parse().unwrap()));
        assert!(config.should_allow_peer(&"10.200.0.2".parse().unwrap()));

        let deny_rules = config.deny;
        config.deny = vec![];

        assert!(config.should_allow_peer(&"127.0.0.1".parse().unwrap()));
        assert!(!config.should_allow_peer(&"127.0.0.2".parse().unwrap()));

        config.allow = vec![];
        config.deny = deny_rules;

        assert!(!config.should_allow_peer(&"127.0.0.1".parse().unwrap()));
        assert!(!config.should_allow_peer(&"127.0.0.2".parse().unwrap()));
        assert!(!config.should_allow_peer(&"10.200.0.1".parse().unwrap()));
        assert!(config.should_allow_peer(&"10.200.0.2".parse().unwrap()));
    }

    #[test]
    fn test_parse_basic_auth() {
        assert_eq!(parse_basic_auth("foo bar"), Ok(("foo", "bar")));
        assert_eq!(
            parse_basic_auth("\"foo\" \"bar baz \""),
            Ok(("foo", "bar baz "))
        );
        assert_eq!(
            parse_basic_auth("foo   \"bar baz\""),
            Ok(("foo", "bar baz"))
        );
        assert_eq!(
            parse_basic_auth("\"foo bar  \"   \"  baz bum\""),
            Ok(("foo bar  ", "  baz bum"))
        );
        assert_eq!(
            parse_basic_auth(""),
            Err("username and password missing".into())
        );
        assert_eq!(parse_basic_auth("foo"), Err("password missing".into()));
        assert_eq!(parse_basic_auth("\"foo\""), Err("password missing".into()));
        assert_eq!(
            parse_basic_auth("\"foo\" bar baz"),
            Err("trailing data after username and password".into())
        );
    }

    #[test]
    fn test_should_allow_credentials() {
        let mut config = Config::default();
        assert!(config.should_allow_credentials(None));
        assert!(config.should_allow_credentials(Some("foo".into())));

        config.basic_auth = Some("foo".into());
        assert!(config.should_allow_credentials(Some("foo".into())));
        assert!(!config.should_allow_credentials(None));
        assert!(!config.should_allow_credentials(Some("bar".into())));
    }
}

use std::io::Error;

use async_std::{io::ReadExt, net::TcpStream};

pub struct TcpStreamReader<'a> {
    pub src: &'a mut TcpStream,
    pub buf: Vec<u8>,
    chunk: Vec<u8>,
    chunk_len: usize,
    eof: bool,
}

impl<'a> TcpStreamReader<'a> {
    pub fn new(src: &'a mut TcpStream) -> Self {
        Self {
            src,
            buf: Vec::with_capacity(4096),
            chunk: vec![0; 512],
            chunk_len: 0,
            eof: false,
        }
    }

    pub async fn read_until(
        &mut self,
        needle: u8,
        max_read: Option<usize>,
    ) -> Result<Vec<u8>, Error> {
        let mut is_slice_ready = self.buf.contains(&needle);
        let mut bytes_read = self.buf.len();

        while !is_slice_ready {
            if let Some(max_read) = max_read {
                if bytes_read >= max_read {
                    return Err(Error::new(
                        std::io::ErrorKind::Interrupted,
                        format!(
                            "max_read of {} exhausted as buf len is {}",
                            max_read,
                            self.buf.len()
                        ),
                    ));
                }
            }

            bytes_read += self.read_chunk().await?;
            is_slice_ready = self.chunk.contains(&needle);
            self.consume_chunk();
        }

        let needle_pos = self.buf.iter().position(|ch| ch == &needle).unwrap();
        if needle_pos == self.buf.len() {
            // whole buffer has to be returned
            self.buf.pop(); // pop needle
            let mut ret = Vec::with_capacity(4096);
            std::mem::swap(&mut ret, &mut self.buf);
            Ok(ret)
        } else {
            // buf: startNrest, N is needle
            // we want to return "start" and keep "rest" in buf
            // split at position of 'r' instead of N, so that rest_buf contains rest and buf contains startN
            // because popping N is cheaper than removing it from the front of the other vec if we were to split at N
            let mut rest_buf = self.buf.split_off(needle_pos + 1);
            std::mem::swap(&mut rest_buf, &mut self.buf);
            let mut ret = rest_buf;
            ret.pop(); // pop needle
            Ok(ret)
        }
    }

    pub async fn skip(&mut self, count: usize) -> Result<(), Error> {
        debug_assert!(count > 0);

        if count < self.buf.len() {
            let mut rest_buf = self.buf.split_off(count);
            std::mem::swap(&mut rest_buf, &mut self.buf);
        } else if count == self.buf.len() {
            self.buf.clear();
        } else {
            let count_skip_from_stream = count - self.buf.len();
            self.buf.clear();
            self.buf.resize(count_skip_from_stream, 0);
            self.src.read_exact(&mut self.buf).await?;
            self.buf.clear();
        }

        Ok(())
    }

    pub async fn peek(&mut self, count: usize) -> Result<&[u8], Error> {
        while self.buf.len() < count {
            self.read_chunk().await?;
            self.consume_chunk();
        }

        Ok(&self.buf[0..count])
    }

    async fn read_chunk(&mut self) -> Result<usize, Error> {
        if self.eof {
            return Err(Error::new(std::io::ErrorKind::UnexpectedEof, "eof"));
        }

        let bytes_read = self.src.read(&mut self.chunk).await?;
        self.chunk_len = bytes_read;
        if bytes_read == 0 {
            self.eof = true;
            return Err(Error::new(std::io::ErrorKind::UnexpectedEof, "eof"));
        }

        Ok(bytes_read)
    }

    fn consume_chunk(&mut self) {
        self.buf.extend_from_slice(&self.chunk[..self.chunk_len]);
        self.chunk_len = 0;
    }
}

snoyproxy
---

An HTTP proxy inspired by [Tinyproxy], implemented largely as an excuse to
experiment with Rust's async features.

HTTP proxying is an easy to use TCP proxying mechanism, well supported out of
the box in Firefox, Chromium, wget, anything built on libcurl, ffmpeg, weechat
etc.

It is not a 1:1 replacement for Tinyproxy. But for the most common use-case of
a transparent proxy, it is a drop-in replacement and will respect your existing
tinyproxy config.

### Quickstart

Refer to the [original quickstart docs for Tinyproxy][tinyproxy-quickstart].

Example config:

```
listen 10.200.200.1
port 8090
connectport 80
connectport 443
basicauth "user name" "pa ss wo rd"
```

Supported config options:

- [x] Port
- [x] Listen
- [x] Timeout
- [x] Allow/Deny (note: hostnames will not be supported)
- [x] BasicAuth
- [x] ConnectPort

Supported CLI options:

- [x] `-c /path/to/config`
    - By default, `/etc/snoyproxy.conf` and `/etc/tinyproxy/tinyproxy.conf` are
      tried in order.
- [x] `-h` for help and `-v` to print version.

Supported signals:

- [x] `SIGUSR1` (for reloading the config file)

### Unsupported

The following features originally present in Tinyproxy are not implemented in
this project.

1. Forwarding to other proxies.
2. Features specific to plaintext-HTTP (reverse proxying, url filtering).
3. Switching to another user/group after binding the socket.
    - Consider running directly as the user you intend to run as.
    - To bind on ports below 1024, consider lowering
      `net.ipv4.ip_unprivileged_port_start` sysctl.
4. Running as a daemon.
    - Consider using a service manager like _runit_ or _openrc_.
5. Logging is currently quite basic and minimal, but might be improved as
   needed.

### Author

Awal Garg <https://awalgarg.me>

### License

This software is hereby released in the public domain.

[Tinyproxy]: https://tinyproxy.github.io/
[tinyproxy-quickstart]: https://tinyproxy.github.io/#quickstart
